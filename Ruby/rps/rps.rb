require 'minitest/autorun'
require 'pry'

class TestRPS < MiniTest::Unit::TestCase

  def setup
    @rps = RPS.new
  end

  def test_computer_throws_a_rock_paper_or_scissors
    possible_throws = ['rock','paper','scissors']
    comp_throw = @rps.comp_throw
    assert_includes(possible_throws, comp_throw)
  end

  def test_paper_beats_rock
    assert_equal(@rps.beats('paper','rock'), true)
  end

  def test_paper_loses_to_scissors
    assert_equal(@rps.beats('paper','scissors'), false)
  end

  def test_paper_loses_to_paper
    assert_equal(@rps.beats('paper','paper'), false)
  end

  def test_rock_loses_to_rock
    assert_equal(@rps.beats('rock','rock'), false)
  end

  def test_rock_beats_scissors
    assert_equal(@rps.beats('rock','scissors'), true)
  end

  def test_rock_loses_to_paper
    assert_equal(@rps.beats('rock','paper'), false)
  end

  def test_scissors_loses_to_rock
    assert_equal(@rps.beats('scissors','rock'), false)
  end

  def test_scissors_loses_to_scissors
    assert_equal(@rps.beats('scissors','scissors'), false)
  end

  def test_scissors_beats_paper
    assert_equal(@rps.beats('scissors','paper'), true)
  end

  def test_score_incremented_by_one
    @rps.adjust_score(true)
    assert_equal(@rps.score, 1)
  end

  def test_score_is_zero_with_a_loss
    @rps.adjust_score(true)
    @rps.adjust_score(false)
    assert_equal(@rps.score, 0)
  end

  def test_score_of_four_increments_wins_by_one
    @rps.adjust_score(true)
    @rps.adjust_score(true)
    @rps.adjust_score(true)
    @rps.adjust_score(true)
    assert_equal(@rps.wins, 1)
  end

  def test_score_goes_to_zero_if_score_is_four
    @rps.adjust_score(true)
    @rps.adjust_score(true)
    @rps.adjust_score(true)
    @rps.adjust_score(true)
    assert_equal(@rps.score, 0)
  end

# Oh, they're supposed to go the other way.

  def test_defeats_increments_by_one
    @rps.adjust_score(false)
    assert_equal(1, @rps.defeats)
  end

  def test_defeats_of_four_increments_losses_by_one
    @rps.adjust_score(false)
    @rps.adjust_score(false)
    @rps.adjust_score(false)
    @rps.adjust_score(false)
    assert_equal(1, @rps.losses)
  end

  def test_defeats_goes_to_zero_if_defeats_is_four
    @rps.adjust_score(false)
    @rps.adjust_score(false)
    @rps.adjust_score(false)
    @rps.adjust_score(false)
    assert_equal(0, @rps.defeats)
  end

  def test_appropriate_winning_message_displayed
    message = @rps.evaluate('rock','scissors')
    assert_equal("Rock against scissors.  You win!", message)
  end

  def test_appropriate_losing_message_displayed
    message = @rps.evaluate('scissors','rock')
    assert_equal("Scissors against rock.  You lose!", message)
  end

end


class RPS
  attr_reader :score, :wins, :defeats, :losses

  def initialize
    @choices_arr = ['rock','paper','scissors']
    @score = 0
    @wins = 0
    @defeats = 0
    @losses = 0
  end

  def play(player_throw)
    c_throw = comp_throw
    adjust_score(beats(player_throw, c_throw))
    evaluate(player_throw, c_throw)
  end

  def comp_throw
    @choices_arr.sample
  end

  def beats(choice1, choice2)
    victory_hash = { 'scissors' => 'paper',
                      'rock' => 'scissors',
                      'paper' => 'rock'}

    victory_hash[choice1] == choice2
  end

  def adjust_score(beats_result)
    if beats_result
      @score += 1
      check_for_win()

    else
      @score = 0
      @defeats += 1
      check_for_loss()
    end
  end

  def check_for_win()
    if @score == 4
      @wins += 1
      @score = 0
    end
  end

  def check_for_loss()
    if @defeats == 4
      @losses += 1
      @defeats = 0
    end
  end

  def evaluate(choice1, choice2)
    beats_result = beats(choice1, choice2)
    win_loss = beats_result ? "win" : "lose"
    choice1.capitalize + " against " + choice2 + ".  You " +  win_loss + "!"

  end


end
