require_relative 'test_helper'

describe 'it works' do
  it 'should return 200' do
    get '/'
    assert last_response.ok?
  end
end

describe 'correct choices presented on index page' do

  it 'should have the RPS choice' do
    get '/'
    last_response.body.must_include "<option value='rps'>Rock, Paper, Scissors</option>"
  end

  it 'should have the RPSD choice' do
    get '/'
    last_response.body.must_include "<option value='rpsd'>Rock, Paper, Scissors, Dynamite</option>"
  end

  it 'should have the RPSLS choice' do
    get '/'
    last_response.body.must_include "<option value='rpsls'>Rock, Paper, Scissors, Lizard, Spock</option>"
  end

  it 'should have the RPSLSD choice' do
    get '/'
    last_response.body.must_include "<option value='rpslsd'>Rock, Paper, Scissors, Lizard, Spock, Dynamite</option>"
  end

end


describe 'correct list of player moves on play page' do

  rock_response = "<input type='radio' name='player_throw' value='rock'>rock<br />"
  paper_response = "<input type='radio' name='player_throw' value='paper'>paper<br />"
  scissors_response = "<input type='radio' name='player_throw' value='scissors'>scissors<br />"
  dynamite_response = "<input type='radio' name='player_throw' value='dynamite'>dynamite<br />"
  lizard_response = "<input type='radio' name='player_throw' value='lizard'>lizard<br />"
  spock_response = "<input type='radio' name='player_throw' value='spock'>spock<br />"

  it 'should  only have Rock Paper or Scissors as player choices on RPS page' do
    get '/choice/?game=rps'
    get '/play/'

    last_response.body.must_include rock_response
    last_response.body.must_include paper_response
    last_response.body.must_include scissors_response
    last_response.body.wont_include dynamite_response
    last_response.body.wont_include lizard_response
    last_response.body.wont_include spock_response

  end

  it 'should  only have Rock Paper Scissors or Dynamite as player choices on RPSD page' do
    get '/choice/?game=rpsd'
    get '/play/'

    last_response.body.must_include rock_response
    last_response.body.must_include paper_response
    last_response.body.must_include scissors_response
    last_response.body.must_include dynamite_response
    last_response.body.wont_include lizard_response
    last_response.body.wont_include spock_response

  end

  it 'should  only have Rock Paper Scissors Lizard or Spock as player choices on RPSLS page' do
    get '/choice/?game=rpsls'
    get '/play/'

    last_response.body.must_include rock_response
    last_response.body.must_include paper_response
    last_response.body.must_include scissors_response
    last_response.body.wont_include dynamite_response
    last_response.body.must_include lizard_response
    last_response.body.must_include spock_response

  end

  it 'should  only have Rock Paper or Scissors as player choices on RPSLSD page' do
    get '/choice/?game=rpslsd'
    get '/play/'

    last_response.body.must_include rock_response
    last_response.body.must_include paper_response
    last_response.body.must_include scissors_response
    last_response.body.must_include dynamite_response
    last_response.body.must_include lizard_response
    last_response.body.must_include spock_response

  end


end
