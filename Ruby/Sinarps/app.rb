require 'bundler'
Bundler.require

game = ''

get '/' do

  erb :index
end

get '/choice/' do
  game_type = params[:game]

  case game_type
  when "rps"
    game = RpslsWakeGem::RPS.new
  when "rpsd"
    game = RpslsWakeGem::RPSD.new
  when "rpsls"
    game = RpslsWakeGem::RPSLS.new
  when "rpslsd"
    game = RpslsWakeGem::RPSLSD.new
  end

   redirect '/play/'
end

get '/play/' do
  @choices = game.choices_arr

  if params[:player_throw]
    @outcome = game.play(params[:player_throw])
  end
    @score = game.score
    @wins = game.wins
    @losses = game.losses
    @defeats = game.defeats
  



  erb :play
end
