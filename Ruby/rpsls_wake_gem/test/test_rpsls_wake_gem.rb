require "test_helper"
require "pry"

class TestRPS < MiniTest::Test

  def setup
    @rps = RpslsWakeGem::RPS.new
    @rpsd = RpslsWakeGem::RPSD.new
    @rpsls = RpslsWakeGem::RPSLS.new
    @rpslsd = RpslsWakeGem::RPSLSD.new

  end

  def test_computer_throws_a_rock_paper_or_scissors
    possible_throws = ['rock','paper','scissors']
    comp_throw = @rps.comp_throw
    assert_includes(possible_throws, comp_throw)
  end

  ##  ROCK   ##

  def test_rock_loses_to_rock
    assert_equal(false, @rps.beats('rock','rock'))
  end

  def test_rock_loses_to_paper
    assert_equal(false, @rps.beats('rock','paper'))
  end

  def test_rock_beats_scissors
    assert_equal(true, @rps.beats('rock','scissors'))
  end

  def test_rock_beats_lizard
    assert_equal(true, @rpsls.beats('rock','lizard'))
  end

  def test_rock_loses_to_spock
    assert_equal(false, @rpsls.beats('rock','spock'))
  end

  def test_rock_loses_to_dynamite
    assert_equal(false, @rpsls.beats('rock','dynamite'))
  end

  ##  PAPER ##

  def test_paper_beats_rock
    assert_equal(true, @rps.beats('paper','rock'))
  end

  def test_paper_loses_to_paper
    assert_equal(false, @rps.beats('paper','paper'))
  end

  def test_paper_loses_to_scissors
    assert_equal(false, @rps.beats('paper','scissors'))
  end

  def test_paper_loses_to_lizard
    assert_equal(false, @rpsls.beats('paper','lizard'))
  end

  def test_paper_beats_spock
    assert_equal(true, @rpsls.beats('paper','spock'))
  end

  def test_paper_loses_to_dynamite
    assert_equal(false, @rpsls.beats('paper','dynamite'))
  end

  ##  SCISSORS  ##

  def test_scissors_loses_to_rock
    assert_equal(false, @rps.beats('scissors','rock'))
  end

  def test_scissors_beats_paper
    assert_equal(true, @rps.beats('scissors','paper'))
  end

  def test_scissors_loses_to_scissors
    assert_equal(false, @rps.beats('scissors','scissors'))
  end

  def test_scissors_beat_lizard
    assert_equal(true, @rpsls.beats('scissors','lizard'))
  end

  def test_scissors_loses_to_spock
    assert_equal(false, @rps.beats('scissors', 'spock'))
  end

  def test_scissors_beat_dynamite
    assert_equal(true, @rpsls.beats('scissors','dynamite'))
  end

  ## LIZARD ##

  def test_lizard_loses_to_rock
    assert_equal(false, @rpsls.beats('lizard','rock'))
  end

  def test_lizard_beats_paper
    assert_equal(true, @rpsls.beats('lizard','paper'))
  end

  def test_lizard_loses_to_scissors
    assert_equal(false, @rpsls.beats('lizard','scissors'))
  end

  def test_lizard_loses_to_lizard
    assert_equal(false, @rpsls.beats('lizard','lizard'))
  end

  def test_lizard_poisons_spock
    assert_equal(true, @rpsls.beats('lizard','spock'))
  end

  def test_lizard_loses_to_dynamite
    assert_equal(false, @rpsls.beats('lizard','dynamite'))
  end

  ## SPOCK  ##

  def test_spock_beats_rock
    assert_equal(true, @rpsls.beats('spock','rock'))
  end

  def test_spock_loses_to_paper
    assert_equal(false, @rpsls.beats('spock','paper'))
  end

  def test_spock_beats_scissors
    assert_equal(true, @rpsls.beats('spock','scissors'))
  end

  def test_spock_loses_to_lizard
    assert_equal(false, @rpsls.beats('spock','lizard'))
  end

  def test_spock_loses_to_spock
    assert_equal(false, @rpsls.beats('spock','spock'))
  end

  def test_spock_beats_dynamite
    assert_equal(true, @rpsls.beats('spock','dynamite'))
  end

  ## DYNAMITE  ##

  def test_dynamite_beats_rock
    assert_equal(true, @rpsd.beats("dynamite", "rock"))
  end

  def test_dynamite_beats_paper
    assert_equal(true, @rpsd.beats('dynamite', 'paper'))
  end

  def test_dynamite_loses_to_scissors
    assert_equal(false, @rpsd.beats("dynamite", "scissors"))
  end

  def test_dynamite_beats_lizard
    assert_equal(true, @rpsd.beats('dynamite', 'lizard'))
  end

  def test_dynamite_loses_to_spock
    assert_equal(false, @rpsd.beats("dynamite", "spock"))
  end

  def test_dynamite_loses_to_dynamite
    assert_equal(false, @rpsd.beats('dynamite', 'dynamite'))
  end

 ##  REST OF THE TESTS ##

  def test_score_incremented_by_one
    @rps.adjust_score(true)
    assert_equal(@rps.score, 1)
  end

  def test_score_is_zero_with_a_loss
    @rps.adjust_score(true)
    @rps.adjust_score(false)
    assert_equal(@rps.score, 0)
  end

  def test_score_of_four_increments_wins_by_one
    @rps.adjust_score(true)
    @rps.adjust_score(true)
    @rps.adjust_score(true)
    @rps.adjust_score(true)
    assert_equal(@rps.wins, 1)
  end

  def test_score_goes_to_zero_if_score_is_four
    @rps.adjust_score(true)
    @rps.adjust_score(true)
    @rps.adjust_score(true)
    @rps.adjust_score(true)
    assert_equal(@rps.score, 0)
  end

  def test_defeats_increments_by_one
    @rps.adjust_score(false)
    assert_equal(1, @rps.defeats)
  end

  def test_defeats_of_four_increments_losses_by_one
    @rps.adjust_score(false)
    @rps.adjust_score(false)
    @rps.adjust_score(false)
    @rps.adjust_score(false)
    assert_equal(1, @rps.losses)
  end

  def test_defeats_goes_to_zero_if_defeats_is_four
    @rps.adjust_score(false)
    @rps.adjust_score(false)
    @rps.adjust_score(false)
    @rps.adjust_score(false)
    assert_equal(0, @rps.defeats)
  end

  def test_appropriate_winning_message_displayed
    message = @rps.evaluate('rock','scissors')
    assert_equal("Rock against scissors.  You win!", message)
  end

  def test_appropriate_losing_message_displayed
    message = @rps.evaluate('scissors','rock')
    assert_equal("Scissors against rock.  You lose!", message)
  end

  def test_dynamite_is_a_valid_choice_in_RPSD
    assert_equal(true, @rpsd.valid_choice('dynamite'))
  end

  def test_dynamite_is_not_a_valid_choice_in_RPS
    assert_equal(false, @rps.valid_choice('dynamite'))
  end

  def test_spock_is_a_valid_choice_in_RPSLS
    assert_equal(true, @rpsls.valid_choice('spock'))
  end

  def test_Spock_capital_S_is_a_valid_choice_in_RPSLS
    assert_equal(true, @rpsls.valid_choice('Spock'))
  end

  def test_lizard_is_valid_choice_in_RPSLS
    assert_equal(true, @rpsls.valid_choice('lizard'))
  end

  def test_lizard_is_invalid_choice_in_RPS
    assert_equal(false, @rps.valid_choice('lizard'))
  end

  def test_appropriate_message_generated_when_invalid_input_is_entered
    message = @rps.play('lizard')
    assert_equal("Invalid choice.  Choice must be one of 'rock','paper','scissors'.", message)
  end

  def test_dynamite_is_a_valid_choice_in_RPSLSD
    assert_equal(true, @rpslsd.valid_choice('dynamite'))
  end

  def test_dynamite_is_an_invalid_choice_in_RPSLS
    assert_equal(false, @rpsls.valid_choice('dynamite'))
  end



  ## TODO test that lizard is valid for RPSLS, invalid for RPS
  ## TODO display message when user enteres invalid choice
  ## TODO set up RPSLSD


end
