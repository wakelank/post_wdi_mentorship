# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rpsls_wake_gem/version'

Gem::Specification.new do |spec|
  spec.name          = "rpsls_wake_gem"
  spec.version       = RpslsWakeGem::VERSION
  spec.authors       = ["wakelank"]
  spec.email         = ["wakelank@gmail.com"]
  spec.summary       = "Play Rock, Paper, Scissors, Lizard, Spock"
  spec.description   = "this is a longer description"
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"

  spec.add_development_dependency "minitest", "~> 5.4.0"
end
