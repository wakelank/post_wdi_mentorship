require "rpsls_wake_gem/version"

module RpslsWakeGem

  class RPS
    attr_reader :score, :wins, :defeats, :losses

    def initialize
      @choices_arr = ['rock','paper','scissors']
      @score = 0
      @wins = 0
      @defeats = 0
      @losses = 0
    end

    def play(player_throw)
      if valid_choice(player_throw)
        c_throw = comp_throw
        adjust_score(beats(player_throw, c_throw))
        evaluate(player_throw, c_throw)
      else
        message = "Invalid choice.  Choice must be one of "
        @choices_arr.each do |choice|
          message += "'" + choice + "'" + ","
        end
        message = message[0..-2]
        message += "."
        return message
      end
    end

    def valid_choice(player_throw)
      @choices_arr.index(player_throw.downcase) ? true : false
    end

    def comp_throw
      @choices_arr.sample
    end

    def beats(choice1, choice2)
      victory_hash = { 'scissors' => ['paper','dynamite','lizard'],
                        'rock' => ['scissors','lizard'],
                        'paper' => ['rock','spock'],
                        'dynamite' => ['paper', 'rock', 'lizard'],
                        'lizard' => ['spock','paper'],
                        'spock' => ['rock','scissors', 'dynamite'] }

      victory_hash[choice1].index(choice2) ? true : false
    end

    def adjust_score(beats_result)
      if beats_result
        @score += 1
        check_for_win()

      else
        @score = 0
        @defeats += 1
        check_for_loss()
      end
    end

    def check_for_win()
      if @score == 4
        @wins += 1
        @score = 0
      end
    end

    def check_for_loss()
      if @defeats == 4
        @losses += 1
        @defeats = 0
      end
    end

    def evaluate(choice1, choice2)
      beats_result = beats(choice1, choice2)
      win_loss = beats_result ? "win" : "lose"
      choice1.capitalize + " against " + choice2 + ".  You " +  win_loss + "!"
    end
  end

  class RPSD < RPS
    def initialize
      @choices_arr = ['rock','paper','scissors','dynamite']
      @score = 0
      @wins = 0
      @defeats = 0
      @losses = 0
    end
  end

  class RPSLS < RPS
    def initialize
      @choices_arr = ['rock','paper','scissors','lizard','spock']
      @score = 0
      @wins = 0
      @defeats = 0
      @losses = 0
    end
  end

  class RPSLSD < RPS
    def initialize
      @choices_arr = ['rock','paper','scissors','spock','lizard','dynamite']
      @score = 0
      @wins = 0
      @defeats = 0
      @losses = 0
    end
  end

end
